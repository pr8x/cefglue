namespace Xilium.CefGlue.Common.Events
{
    public delegate void RenderProcessUnhandledExceptionEventHandler(object sender, RenderProcessUnhandledExceptionEventArgs e);
}
