using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Threading;
using Xilium.CefGlue.Common.Helpers;
using Xilium.CefGlue.Common.Helpers.Logger;

namespace Xilium.CefGlue.WPF
{
    /// <summary>
    /// The WPF builtin render 
    /// </summary>
    internal class WpfRenderHandler : BuiltInRenderHandler
    {
        private WriteableBitmap _bitmap;

        public WpfRenderHandler(Image image, ILogger logger) : base(logger)
        {
            Image = image;
        }

        public Image Image { get; }

        public bool AllowsTransparency { get; set; } = true;

        private PixelFormat PixelFormat => AllowsTransparency ? PixelFormats.Bgra32 : PixelFormats.Bgr32;

        protected override int BytesPerPixel => PixelFormat.BitsPerPixel / 8;

        protected override bool UpdateDirtyRegionsOnly => true;

        protected override void CreateBitmap(int width, int height)
        {
            _bitmap = new WriteableBitmap(width, height, Dpi, Dpi, PixelFormat, null);
            Image.Source = _bitmap;
        }

        protected override Action BeginBitmapUpdate()
        {
            _bitmap.Lock();
            return () => _bitmap.Unlock();
        }

        protected override void ExecuteInUIThread(Action action)
        {
            Image.Dispatcher.BeginInvoke(DispatcherPriority.Render, action);
        }

        protected override int RenderedHeight =>  _bitmap?.PixelHeight ?? 0;

        protected override int RenderedWidth => _bitmap?.PixelWidth ?? 0;

        protected override void UpdateBitmap(IntPtr sourceBuffer, int sourceBufferSize, int stride, CefRectangle updateRegion)
        {
            var sourceRect = new Int32Rect(updateRegion.X, updateRegion.Y, updateRegion.Width, updateRegion.Height);
            _bitmap.WritePixels(sourceRect, sourceBuffer, sourceBufferSize, stride, updateRegion.X, updateRegion.Y);
        }
    }
}
